# ZMI_TEST_BACKEND

Proyek ini adalah contoh proyek untuk demonstrasi.

## Deskripsi

Proyek ini bertujuan untuk mengilustrasikan penggunaan teknologi ExpressJS dalam konteks manajemen produk.

## Fitur

- API CRUD Product
- API CRUD Category
- API CRUD Variant
- API CRUD Image

## Instalasi

1. Clone repositori ini ke mesin lokal Anda.
2. Buka terminal dan masuk ke direktori proyek.
3. Jalankan perintah `npm install` untuk menginstal dependensi.
4. Jalankan perintah `npm start` atau `npm run dev` untuk menjalankan proyek.
5. Pastikan menggunakan versi node >= 16 dan npm >= 8

## Kontribusi

Kami terbuka untuk kontribusi dari komunitas. Jika Anda ingin berkontribusi, silakan ikuti langkah-langkah berikut:

1. Fork repositori ini.
2. Buat branch baru dengan fitur/peningkatan yang ingin Anda tambahkan.
3. Lakukan perubahan yang diperlukan.
4. Ajukan pull request ke branch utama.

## Lisensi

Proyek ini dilisensikan di bawah Lisensi MIT. Lihat file [LICENSE](LICENSE) untuk detail lebih lanjut.
