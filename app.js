const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const app = express()

const v1 = '/api/v1'

const categoriesRouter = require('./app/api/v1/categories/router')
const variantsRouter = require('./app/api/v1/variants/router')
const imagesRouter = require('./app/api/v1/images/router')
const productsRouter = require('./app/api/v1/products/router')

const notFoundMiddleware = require('./app/middleware/not-found')
const handleErrorMiddleware = require('./app/middleware/handler-error')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
    res.status(200).json({ status: 'success', message: 'Welcome to API ZMI_Test_Backend' })
})
app.use(`${v1}`, categoriesRouter)
app.use(`${v1}`, variantsRouter)
app.use(`${v1}`, imagesRouter)
app.use(`${v1}`, productsRouter)

app.use(notFoundMiddleware)
app.use(handleErrorMiddleware)

module.exports = app
