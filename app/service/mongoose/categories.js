const Categories = require('../../api/v1/categories/model')
const { BadRequestError, NotFoundError } = require('../../errors')

const createCategories = async(req) => {
    const { name } = req.body

    const check = await Categories.findOne({ name })
    if(check) throw new BadRequestError('category name already exists')
    
    const result = await Categories.create({ name })
    return result
}

const getAllCategories = async() => {
    const result = await Categories.find()
    return result
}

const getOneCategoies = async(req) => {
    const { id } = req.params

    const result = await Categories.findById(id)
    if(!result) throw new NotFoundError('category not found')

    return result
}

const updateCategories = async(req) => {
    const { id } = req.params
    const { name } = req.body

    const check = await Categories.findOne({ name, _id: { $ne: id }})
    if(check) throw new BadRequestError('category name already exists')

    const result = await Categories.findOneAndUpdate({ _id: id }, { name }, { new: true, runValidators: true })
    if(!result) throw new NotFoundError(`update failed category not found`)

    return result
}

const deleteCategorie = async(req) => {
    const { id } = req.params

    const result = await Categories.findByIdAndRemove(id)
    if(!result) throw new NotFoundError('delete failed category not found')

    return result
}

const checkingCategories = async(id) => {
    const result = await Categories.findById(id)
    if(!result) throw new NotFoundError('category not found')
    return result
}

module.exports = { createCategories, getAllCategories, getOneCategoies, updateCategories, deleteCategorie, checkingCategories }