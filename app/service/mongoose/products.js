const Products = require('../../api/v1/products/model')
const Variants = require('../../api/v1/variants/model')
const Images = require('../../api/v1/images/model')
const fs = require('fs')
const { NotFoundError } = require('../../errors')
const { checkingCategories } = require('./categories')

const getAllProducts = async(req) => {
    const { keyword, category } = req.query
    let condition = {}

    if(keyword) {
        condition = {...condition, name: { $regex: keyword, $options: 'i' }}
    }

    if(category) {
        condition = {...condition, category}
    }

    const result = await Products.find(condition)
    .populate({ path: 'image', select: '_id name url' })

    return result
}

const getOneProduct = async(req) => {
    const { id } = req.params

    const result = await Products.findById(id)
    .populate({ path: 'category', select: '_id name' })
    .populate({ path: 'variant', select: '_id name value' })
    .populate({ path: 'image', select: '_id name url' })

    if(!result) throw new NotFoundError('product not found')

    return result
}

const createProducts = async(req) => {
    const { name, description, price, variant, category, image } = req.body
    
    await checkingCategories(category)

    const result = await Products.create({ name, description, price, variant, category, image })
    
    await Images.updateMany({ _id: { $in: image } }, { $set: { product: result._doc._id } })
    await Variants.updateMany({ _id: { $in: variant } }, { $set: { product: result._doc._id } })

    return result
}

const updateProduct = async(req) => {
    const { id } = req.params
    const { name, description, price, variant, category, image } = req.body

    await checkingCategories(category)

    const result = await Products.findOneAndUpdate(
        {_id: id},
        {name, description, price, variant, category, image},
        {new: true, runValidators: true}
    )
    if(!result) throw new NotFoundError('update failed product not found')

    await Variants.updateMany({ product: result._doc._id }, { $unset: { product: '' } })
    await Images.updateMany({ product: result._doc._id }, { $unset: { product: '' } })
    
    await Variants.updateMany({ _id: { $in: variant } }, { $set: { product: result._doc._id } })
    await Images.updateMany({ _id: { $in: image } }, { $set: { product: result._doc._id } })

    return result
}

const deleteProduct = async(req) => {
    const { id } = req.params

    const check = await Products.findById(id)
    .populate({path: 'image', select: '_id name'})
    if(!check) throw new NotFoundError('delete failed product not found')

    check._doc.variant.map(async(item) => await Variants.findByIdAndRemove(item))
    check._doc.image.map(async(item) => {
        await Images.findByIdAndRemove(item._id)
        fs.unlink(`public/images/${item.name}`, err => console.log(err))
    })

    const result = await Products.findByIdAndRemove(id)

    return result
}

module.exports = { getAllProducts, getOneProduct, createProducts, updateProduct, deleteProduct }