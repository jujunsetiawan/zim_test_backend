const Variants = require('../../api/v1/variants/model')
const { BadRequestError, NotFoundError } = require('../../errors')

const getAllVariants = async() => {
    const result = await Variants.find()
    return result
}

const getOneVariant = async(req) => {
    const { id } = req.params

    const result = await Variants.findById(id)
    if(!result) throw new NotFoundError('variant not found')

    return result
}

const createVariants = async(req) => {
    const { name, value } = req.body

    const result = await Variants.create({ name, value })
    return result
}

const updateVariants = async(req) => {
    const { id } = req.params
    const { name, value } = req.body

    const result = await Variants.findOneAndUpdate(
        {_id: id},
        {name, value},
        {new: true, runValidators: true}
    )

    if(!result) throw new NotFoundError('variant not found')

    return result
}

const deleteVariants = async(req) => {
    const { id } = req.params
    
    const result = await Variants.findByIdAndRemove(id)
    if(!result) throw new NotFoundError('variant not found')

    return result
}

const checkingVariant = async(id) => {
    const result = await Variants.findById(id)
    if(!result) throw new NotFoundError('variant not found')
    return result
} 

module.exports = { createVariants, getAllVariants, getOneVariant, updateVariants, deleteVariants, checkingVariant }