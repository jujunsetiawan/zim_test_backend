const Images = require('../../api/v1/images/model')
const { NotFoundError } = require('../../errors')
const fs = require('fs')

const generatorUrlImage = async(req) => {
  const result = `images/${req.file.filename}`
  return result
}

const createImages = async (req) => {
  const result = await Images.create({
    name: `${req?.file?.filename}`,
    url: req.file ? `http://localhost:${process.env.PORT}/images/${req?.file?.filename}` : ''
  });

  return result
}

const updateImages = async(req) => {
    const { id } = req.params

    const check = await Images.findById(id)
    if(!check) throw new NotFoundError('update failed image not found')

    fs.unlink(`public/images/${check._doc.name}`, err => console.log(err))

    const result = await Images.findOneAndUpdate(
        {_id: id},
        {
          name: `${req?.file?.filename}`,
          url: req.file ? `http://localhost:${process.env.PORT}/images/${req?.file?.filename}` : ''
        },
        {new: true, runValidators: true}
    );
    
    return result
}

const deleteImages = async(req) => {
    const { id } = req.params

    const check = await Images.findById(id)
    if(!check) throw new NotFoundError('delete failed image not found')

    fs.unlink(`public/images/${check._doc.name}`, err => console.log(err))

    const result = await Images.findByIdAndRemove(id)
    return result
}

const checkingImage = async (id) => {
    const result = await Images.findOne({ _id: id })
    if (!result) throw new NotFoundError('image product not found')
  
    return result
  }

module.exports = { createImages, checkingImage, generatorUrlImage, deleteImages, updateImages }