const mongoose = require('mongoose');
const { urlDb } = require('../config');

mongoose.connect(urlDb)
.then(() => console.log('mongodb connected'))
.catch(err => console.log(err))

const db = mongoose.connection;

module.exports = db;