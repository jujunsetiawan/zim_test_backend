const { createVariants, deleteVariants, getAllVariants, getOneVariant, updateVariants } = require('../../../service/mongoose/variants')
const { StatusCodes } = require('http-status-codes')

const index = async(req, res, next) => {
    try {
        const result = await getAllVariants()
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const find = async(req, res, next) => {
    try {
        const result = await getOneVariant(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const create = async(req, res, next) => {
    try {
        const result = await createVariants(req)
        res.status(StatusCodes.CREATED).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const update = async(req, res, next) => {
    try {
        const result = await updateVariants(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const remove = async(req, res, next) => {
    try {
        await deleteVariants(req)
        res.status(StatusCodes.OK).json({ status: 'success', message: 'variant deleted successfully' })
    } catch (err) {
        next(err)
    }
}

module.exports = { index, find, create, update, remove }