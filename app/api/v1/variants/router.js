const router = require('express').Router()
const { create, find, index, remove, update } = require('./controller')

router.get('/variants', index)
router.post('/variants', create)
router.get('/variants/:id', find)
router.put('/variants/:id', update)
router.delete('/variants/:id', remove)

module.exports = router