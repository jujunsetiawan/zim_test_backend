const mongoose = require('mongoose')
const { Schema, model } = mongoose

const variantValueSchema = Schema(
    {
        type: {
            type: String,
            required: [true, 'type cannot be empty']
        },
        stock: {
            type: Number,
            required: [true, 'stock cannot be empty']
        }
    }
)

const variantSchema = Schema(
    {
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Product'
        },
        name: {
            type: String,
            required: [true, 'name cannot be empty']
        },
        value: {
            type: [variantValueSchema],
            required: [true, 'value cannot be empty']
        }
    },
    { timestamps: true }
)

module.exports = model('Variant', variantSchema)