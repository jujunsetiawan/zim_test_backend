const { createCategories, getAllCategories, getOneCategoies, deleteCategorie, updateCategories } = require('../../../service/mongoose/categories')
const { StatusCodes } = require('http-status-codes')

const index = async(req, res, next) => {
    try {
        const result = await getAllCategories()
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const find = async(req, res, next) => {
    try {
        const result = await getOneCategoies(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const create = async(req, res, next) => {
    try {
        const result = await createCategories(req)
        res.status(StatusCodes.CREATED).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const update = async(req, res, next) => {
    try {
        const result = await updateCategories(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const remove = async(req, res, next) => {
    try {
        await deleteCategorie(req)
        res.status(StatusCodes.OK).json({ status: 'success', message: 'category deleted successfully' })
    } catch (err) {
        next(err)
    }
}

module.exports = { index, find, create, remove, update }