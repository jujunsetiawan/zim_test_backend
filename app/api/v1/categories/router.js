const router = require('express').Router()
const { create, find, index, remove, update } = require('./controller')

router.get('/categories', index)
router.post('/categories', create)
router.get('/categories/:id', find)
router.put('/categories/:id', update)
router.delete('/categories/:id', remove)

module.exports = router