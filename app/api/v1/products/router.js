const router = require('express').Router()
const { create, find, index, update, remove } = require('./controller')

router.get('/products', index)
router.post('/products', create)
router.get('/products/:id', find)
router.put('/products/:id', update)
router.delete('/products/:id', remove)

module.exports = router