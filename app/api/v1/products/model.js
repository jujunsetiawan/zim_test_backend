const mongoose = require('mongoose')
const { Schema, model } = mongoose

const productSchema = Schema(
    {
        name: {
            type: String,
            required: [true, 'name cannot be empty']
        },
        description: {
            type: String,
            required: [true, 'description cannot be empty']
        },
        price: {
            type: Number,
            required: [true, 'price cannot be empty']
        },
        variant: [{
            type: mongoose.Types.ObjectId,
            ref: 'Variant',
            required: true
        }],
        image: [{
            type: mongoose.Types.ObjectId,
            ref: 'Image',
            required: true
        }],
        category: {
            type: mongoose.Types.ObjectId,
            ref: 'Category',
            required: true
        }
    },
    { timestamps: true }
)

module.exports = model('Product', productSchema)