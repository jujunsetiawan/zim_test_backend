const { createProducts, getAllProducts, getOneProduct, updateProduct, deleteProduct } = require('../../../service/mongoose/products')
const { StatusCodes } = require('http-status-codes')

const index = async(req, res, next) => {
    try {
        const result = await getAllProducts(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const find = async(req, res, next) => {
    try {
        const result = await getOneProduct(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const create = async(req, res, next) => {
    try {
        const result = await createProducts(req)
        res.status(StatusCodes.CREATED).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const update = async(req, res, next) => {
    try {
        const result = await updateProduct(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (err) {
        next(err)
    }
}

const remove = async(req, res, next) => {
    try {
        await deleteProduct(req)
        res.status(StatusCodes.OK).json({ status: 'success', message: 'product deleted successfully' })
    } catch (err) {
        next(err)
    }
}

module.exports = { create, index, find, update, remove }