const { createImages, deleteImages, updateImages } = require('../../../service/mongoose/images')
const { StatusCodes } = require('http-status-codes')

const create = async(req, res, next) => {
    try {
        const result = await createImages(req)
        res.status(StatusCodes.CREATED).json({ status: 'success', data: result })
    } catch (error) {
        next(error)
    }
}

const update = async(req, res, next) => {
    try {
        const result = await updateImages(req)
        res.status(StatusCodes.OK).json({ status: 'success', data: result })
    } catch (error) {
        next(error)
    }
}

const remove = async(req, res, next) => {
    try {
        await deleteImages(req)
        res.status(StatusCodes.OK).json({ status: 'success', message: 'image deleted succesfully' })
    } catch (error) {
        next(error)
    }
}

module.exports = { create, update, remove }