const router = require('express').Router()
const { create, remove, update } = require('./controller')
const upload = require('../../../middleware/multer')

router.post('/images', upload.single('product_image'), create)
router.put('/images/:id', upload.single('product_image'), update)
router.delete('/images/:id', remove)

module.exports = router