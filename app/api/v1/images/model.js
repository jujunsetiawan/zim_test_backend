const mongoose = require('mongoose')
const { Schema, model } = mongoose

const imageSchema = Schema(
    {
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Product'
        },
        name: {
            type: String,
            required: [true, 'product image cannot empty']
        },
        url: {
            type: String,
            required: [true, 'product image cannot empty']
        }
    },
    { timestamps: true }
)

module.exports = model('Image', imageSchema)